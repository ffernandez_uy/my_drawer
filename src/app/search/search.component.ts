import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Store } from "@ngrx/store";
import * as SocialShare from "nativescript-social-share";
import { compose } from "nativescript-email"; 
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application, Color, Page, View } from "@nativescript/core";
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.model";
import { NoticiasService } from "../domain/noticias.service";
import { DURATION, show, ToastOptions } from "nativescript-toasts";
import { AppState } from "../app.module";

@Component({
    selector: "Search",
    moduleId: module.id,
    templateUrl: "./search.component.html"
})
export class SearchComponent implements OnInit {
    idx: number = 5;
    resultados: Array<String> = [];
    @ViewChild("layout") layout: ElementRef;

    constructor(
        public noticias: NoticiasService,
        private store: Store<AppState>
        ) {
        // Use the component constructor to inject providers.
        //this.noticias.agregar("hola 1!");
        //this.noticias.agregar("hola 2!");
        //this.noticias.agregar("hola 3!");
        //this.noticias.agregar("hola 4!");
    }

    ngOnInit(): void {
        this.store.select((state) => state.noticias.sugerida)
            .subscribe((data) => {
                const f = data;
                if (f != null) {
                    const toastsOptions: ToastOptions = {text: "Sugerimos leer: " + f.titulo, duration: DURATION.SHORT};
                    show(toastsOptions);
                }
            });
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args): void {
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }

    onLongPress(s): void {
        console.log(s);
        //SocialShare.shareText(s, "Asunto: Compartido desde el curso!");

        const fs = require("file-system"); // usas el filesystem para adjuntar un archivo
        const appFolder = fs.knownFolders.currentApp(); // esto te da un objeto de tipo Folder
        const appPath = appFolder.path; // esto te da el path a la carpeta src
        const logoPath = appPath + "/assets/images/icon_for_mail.png"; // aquí armas el path del archivo copiado
        compose({
            subject: "Mail de Prueba", // asunto del mail
            body: "Hola <strong>mundo!</strong> :)", // cuerpo que será enviado
            to: ["fernando.fernandez.racedo@gmail.com"], //lista de destinatarios principales
            cc: [], //lista de destinatarios en copia
            bcc: [], //lista de destinatarios en copia oculta
            attachments: [ //listado de archivos adjuntos
                {
                    fileName: "arrow1.png", // este archivo adjunto está en formato base 64 representado por un string
                    path: "base64://iVBORw0KGgoAAAANSUhEUgAAABYAAAAoCAYAAAD6xArmAAAACXBIWXMAABYlAAAWJQFJUiTwAAAAHGlET1QAAAACAAAAAAAAABQAAAAoAAAAFAAAABQAAAB5EsHiAAAAAEVJREFUSA1iYKAimDhxYjwIU9FIBgaQgZMmTfoPwlOmTJGniuHIhlLNxaOGwiNqNEypkwlGk9RokoIUfaM5ijo5Clh9AAAAAP//ksWFvgAAAEFJREFUY5g4cWL8pEmT/oMwiM1ATTBqONbQHA2W0WDBGgJYBUdTy2iwYA0BrILDI7VMmTJFHqv3yBUEBQsIg/QDAJNpcv6v+k1ZAAAAAElFTkSuQmCC",
                    mimeType: "image/png"
                },
                {
                    fileName: "icon_for_mail.png", // este archivo es el que lees directo del filesystem del mobile
                    path: logoPath,
                    mimeType: "image/png"
                }
            ]
        }).then(() => console.log("Enviador de mail cerrado"), (err) => console.log("Error: " + err));
    }

    onPull(e) {
        console.log(e);
        const pullRefresh = e.object;
        setTimeout(() => {
            this.noticias.agregar("hola " + this.idx + "! (Add by pull&refresh)");
            this.idx++;
            pullRefresh.refreshing = false;
        }, 2000);
    }

    buscarAhora(s: string) {
        console.dir("buscarAhora " + s);
        this.noticias.buscar(s).then( (r: any) => {
            console.log("resultados buscarAhora: " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("error buscarAhora: " + e);
            
            const toastsOptions: ToastOptions = {text: "Error en la búsqueda", duration: DURATION.SHORT};
            show(toastsOptions);
        });
/*
        const layout = <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            opacity: 0.5,
            rotate: 180,
            duration: 750,
            delay: 150
        }).then(() => layout.animate({
            backgroundColor: new Color("white"),
            opacity: 1,
            rotate: 0,
            duration: 750,
            delay: 150
        }));
*/
    }
}
