import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule, NativeScriptFormsModule } from "@nativescript/angular";

import { SearchFromComponent } from "./search-form.component";
import { SearchRoutingModule } from "./search-routing.module";
import { SearchComponent } from "./search.component";
import { MinLenDirective } from "./minlen.validator";
import { DetalleComponent } from "../detalle/detalle.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        SearchRoutingModule,
        NativeScriptFormsModule
    ],
    declarations: [
        SearchComponent,
        DetalleComponent,
        SearchFromComponent,
        MinLenDirective
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SearchModule { }

