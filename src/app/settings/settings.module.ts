
import { NativeScriptCommonModule, NativeScriptFormsModule } from "@nativescript/angular";

import { SettingsRoutingModule } from "./settings-routing.module";
import { SettingsComponent } from "./settings.component";
import { SettingsUsername } from "./settings.username";
import { MinLenDirectiveUserName } from "./minlen.validator.username";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        SettingsRoutingModule,
        NativeScriptFormsModule
    ],
    declarations: [
        SettingsComponent,
        SettingsUsername,
        MinLenDirectiveUserName
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SettingsModule { }
