import { Directive, Input } from"@angular/core";
import { AbstractControl, NG_VALIDATORS, Validator } from"@angular/forms";

@Directive({
    selector: "[minlenusername]",
    providers: [{
        provide: NG_VALIDATORS,
        useExisting: MinLenDirectiveUserName,
        multi: true
    }]
})

export class MinLenDirectiveUserName implements Validator {
    @Input() minlen: string;
    
    constructor() {
        //
    }
    
    validate(control: AbstractControl): {[key: string]: any} {
        return !control.value || control.value.length >= (this.minlen || 2) ? null : { minlen: true };
     }
} 