import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";

import { LinksRoutingModule } from "./links-routing.module";
import { LinksComponent } from "./links.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        LinksRoutingModule
    ],
    declarations: [
        LinksComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class LinksModule { }