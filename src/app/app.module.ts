import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "@nativescript/angular";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";

import { EffectsModule } from "@ngrx/effects";
import { ActionReducerMap, StoreModule as NgRxSoreModule } from "@ngrx/store";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import {
    initializeNoticiasState,
    NoticiasEffects,
    NoticiasState,
    reducersNoticias
} from "./domain/noticias-state.model";
import { NoticiasService } from "./domain/noticias.service";

// redux init
// tslint:disavle-next-line:interface-name
export interface AppState {
    noticias: NoticiasState;
}

const reducers: ActionReducerMap<AppState> = {
    noticias: reducersNoticias
};

const reducersInitialState = {
    noticias: initializeNoticiasState()
};
// fin redux init

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        NativeScriptModule,
        NativeScriptUISideDrawerModule,
        NgRxSoreModule.forRoot(reducers, {initialState: reducersInitialState }),
        EffectsModule.forRoot([NoticiasEffects])
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        NoticiasService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
